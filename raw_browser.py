#!/usr/bin/python3

"""
PyQt5 Raw Browser for HTML/JSON

Author: Nick Santos
"""

import sys

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtWebEngineWidgets import *


app = QApplication([])

address_bar = QLineEdit()
search_button = QPushButton('Search')
browser = QWebEngineView()

def clicked_or_return():
    page = (address_bar.text())
    browser.load(QUrl(('view-source:' + page)))
    browser.show()

search_button.clicked.connect(clicked_or_return)
address_bar.returnPressed.connect(clicked_or_return)

grid = QGridLayout()
grid.addWidget(address_bar, 1, 0)
grid.addWidget(search_button, 1, 1)
grid.addWidget(browser, 2, 0)

window = QWidget()
window.setGeometry(100, 100, 800, 600)
window.setLayout(grid)
window.setWindowTitle('Raw Browser')
window.show()

app.exec_()
